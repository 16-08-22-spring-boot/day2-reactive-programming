package com.classpath.day2reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day2ReactiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(Day2ReactiveApplication.class, args);
    }

}
