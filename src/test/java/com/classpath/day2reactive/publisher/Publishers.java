package com.classpath.day2reactive.publisher;

import java.time.Duration;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class Publishers {

	@Test
	public void testMonoOperator() {
		Mono<String> data = Mono.just("one");
		// System.out.println(data);
		data.subscribe(d -> System.out.println(d));
	}

	@Test
	public void testFluxOperator() {
		Flux<Integer> integerFlux = Flux.just(11, 22, 33, 44, 55, 66, 77, 88);
		integerFlux.subscribe(number -> System.out.println(number));
	}

	@Test
	public void testFluxOperatorOnComplete() {
		Flux<Integer> integerFlux = Flux.just(11, 22, 33, 44, 55, 66, 77, 88);
		integerFlux.subscribe(data -> System.out.println(data), err -> {
		}, () -> System.out.println("Flux consumption is complete"));
	}

	@Test
	public void testSubsciption() {
		Flux<Integer> integerFlux = Flux.just(11, 22, 33, 44, 55, 66, 77, 88);
		integerFlux.subscribe(new Subscriber<Integer>() {

			private Subscription subscription = null;

			@Override
			public void onSubscribe(Subscription subscription) {
				this.subscription = subscription;
				subscription.request(2);
			}

			@Override
			public void onNext(Integer data) {
				System.out.println(data);
				System.out.println("Lets request few more data");
				if (data == 44) {
					this.subscription.cancel();
				} else {
					this.subscription.request(2);
				}

			}

			@Override
			public void onError(Throwable t) {
				System.out.println("exception");

			}

			@Override
			public void onComplete() {
				System.out.println("completed....");

			}

		});
	}
	
	@Test
	public void testMapOperator() {
		Flux<Integer> integerFlux = Flux.just(11, 22, 33, 44, 55, 66, 77, 88);
		Flux<Integer> flux3Multiplier = integerFlux
										.map(data -> data * 3)
										.filter(data -> data % 2 == 0);
		flux3Multiplier.subscribe(value -> System.out.println(value));
	}

	public static Flux<String> firstNamePublisher() {
		return Flux.fromIterable(List.of("Ramesh", "Vijay", "Harish"))
				.concatWith(Flux.error(new RuntimeException("error"))).onErrorResume(e -> Flux.just("Vinod"))
				.delayElements(Duration.ofMillis(100));
	}

	public static Flux<String> lastNamePublisher() {
		return Flux.fromIterable(List.of("Kumar", "Mehta", "Rao", "Mehta")).delayElements(Duration.ofMillis(200));
	}

	public static Flux<String> mergeNames = Flux
			.zip(firstNamePublisher(), lastNamePublisher(), (fName, lName) -> fName + " - " + lName)
			.onErrorReturn("Exception while concatanating the names");

}